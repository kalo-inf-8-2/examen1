package com.example.rectangulo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RectanguloActivity extends AppCompatActivity {

    private TextView txtNombre;
    private EditText txtBase;
    private EditText txtAltura;
    private TextView txtArea;
    private TextView txtPerimetro;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private Rectangulo rec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rectangulo);

        txtNombre = (TextView) findViewById(R.id.txtNombre2);
        txtBase = (EditText) findViewById(R.id.txtBase);
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtArea= (TextView) findViewById(R.id.txtArea);
        txtPerimetro = (TextView) findViewById(R.id.txtPerimetro);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        txtNombre.setText("Mi nombre es: " + nombre);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String base = txtBase.getText().toString();
                String altura = txtAltura.getText().toString();
                if(base.matches("") || altura.matches("")) {
                    Toast.makeText(RectanguloActivity.this, "Inserta valores correctos", Toast.LENGTH_SHORT).show();
                } else {
                    rec = new Rectangulo(Integer.valueOf(base), Integer.valueOf(altura));
                    txtArea.setText(rec.calcularCalcularArea().toString());
                    txtPerimetro.setText(rec.calcularPagPerimetro().toString());
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtBase.setText("");
                txtAltura.setText("");
                txtArea.setText("");
                txtPerimetro.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
