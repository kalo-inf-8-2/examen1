package com.example.rectangulo;

public class Rectangulo {

    private Integer base;
    private Integer altura;

    public Rectangulo(Integer base, Integer altura) {
        this.base = base;
        this.altura = altura;
    }

    public Float calcularCalcularArea() {
        return Float.valueOf(this.base * this.altura);
    }

    public Float calcularPagPerimetro() {
        return 2f * this.altura + 2f * this.base;
    }
}
